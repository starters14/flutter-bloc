import 'package:flutter/material.dart';
import 'features/home/home.dart';

class App extends MaterialApp{
  const App({Key? key}) : super (key:key, home: const HomePage());
}