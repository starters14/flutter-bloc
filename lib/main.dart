import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/widgets.dart';

import 'app.dart';

void main() {
  BlocOverrides.runZoned(() => runApp(const App()),
  blocObserver: null,
  );
}